package com.jk.model;

import lombok.Data;

@Data
public class UserBean {

    private Integer userId;

    private String userName;

    private String userAge;
}
